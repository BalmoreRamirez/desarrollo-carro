<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GraficasMol extends CI_Model
{

	public function getVenta()
	{
	$query = $this->db->query('select fa.num_factura,ven.nombre,count(fa.num_factura) as fa from tab_factura  fa
								inner join tab_vendedor ven on fa.id_vendedor =  ven.id_vendedor 
								group by fa.id_vendedor');						
		return $query->result();
	}

}